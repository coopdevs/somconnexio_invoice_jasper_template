# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
## [0.3.2] - 2022-04-04
- Show addres for each BA subscription[#57](https://gitlab.com/som-connexio/somconnexio_invoice_jasper_template/-/merge_requests/57)

## [0.3.1] - 2020-05-25
- Only show the taxes colum in the invoice lines if the customer is 7070 or 15685 (HACK) [#56](https://gitlab.com/som-connexio/somconnexio_invoice_jasper_template/-/merge_requests/56)

## [0.3.0] - 2020-04-23
### Added
- Add base and labels to the taxes table [#46](https://gitlab.com/som-connexio/somconnexio_invoice_jasper_template/-/merge_requests/46)
- Order the invoice lines [#49](https://gitlab.com/som-connexio/somconnexio_invoice_jasper_template/-/merge_requests/49)
- Add taxes column to the invoice lines [#50](https://gitlab.com/som-connexio/somconnexio_invoice_jasper_template/-/merge_requests/53) and [#53](https://gitlab.com/som-connexio/somconnexio_invoice_jasper_template/-/merge_requests/53)
- Only show the taxes colum in the invoice lines if the customer is 7070 (HACK) [#54](https://gitlab.com/som-connexio/somconnexio_invoice_jasper_template/-/merge_requests/54)

### Changed
- Redesign the Invoice lines subreport [#47](https://gitlab.com/som-connexio/somconnexio_invoice_jasper_template/-/merge_requests/47)

### Fixed
- Order details by date [#48](https://gitlab.com/som-connexio/somconnexio_invoice_jasper_template/-/merge_requests/48)
- Print invoice total table always [#51](https://gitlab.com/som-connexio/somconnexio_invoice_jasper_template/-/merge_requests/51)

### Deprecated
### Removed
- Removed XML script. Moved to a new dedicated repository [#52](https://gitlab.com/som-connexio/somconnexio_invoice_jasper_template/-/merge_requests/52)

### Security

## [0.2.7] - 2020-03-20
### Added
- Document how to use adapters to preview the template [#44](https://gitlab.com/som-connexio/somconnexio_invoice_jasper_template/-/merge_requests/44)
### Fixed
- Expand header of invoice details table to all the columns [#45](https://gitlab.com/som-connexio/somconnexio_invoice_jasper_template/-/merge_requests/45)

## [0.2.6] - 2020-03-02
### Removed
- Line service from Invoice details subreport [#38](https://gitlab.com/som-connexio/somconnexio_invoice_jasper_template/-/merge_requests/38)
### Fixed
- Change uses of Line Consume translation [#36](https://gitlab.com/som-connexio/somconnexio_invoice_jasper_template/-/merge_requests/36)

## [0.2.5] - 2020-01-27
## [0.2.4] - 2020-12-05
## [0.2.3] - 2019-12-05
## [0.2.2] - 2019-10-11
## [0.2.1] - 2019-10-11
## [0.1.3] - 2019-10-07
## [0.1.2] - 2019-09-19
## [0.1.1] - 2019-09-16
## [0.1.0] - 2019-09-06
## [0.0.4] - 2019-09-02
## [0.0.3] - 2019-08-09
## [0.0.2] - 2019-07-29
## [0.0.1] - 2019-07-24
## [0.0.0] - 2019-07-04
