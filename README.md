# JasperStudio project for create the Som Connexió invoice template

This repository is a [JasperStudio](https://community.jaspersoft.com/project/jaspersoft-studio) project used to generate the `jasper` file used in OpenCell to generate the invoices.

This project work with:

* JasperStudio 6.8.0 and 6.9.0
* OpenCell 6.1.0, 6.2.0 and 6.3.0

## Requirements

 * To redesign and build: JasperStudio >=6.8.0
 * To generate the invoice: OpenCell >= 6.1.0 with Admin or SuperAdmin access

## Setup

 * Install JasperStudio from https://community.jaspersoft.com/project/jaspersoft-studio
 * Clone this repository
 * Import the project: `File/Import...`.
 * Select import from local git project: `Git/Projects from Git/Existing local repository`. In the next windows `Add` a new project searching the location in our file system.
 * Start to edit the template...

## Test the template

Using a XML invoice file from OpenCell, we can test our template to debug the possible bugs and check the performance.

Following the next tutorial: https://community.jaspersoft.com/wiki/xml-datasources-jaspersoft-studio we can generate the invoice PDF.

## Build the `jasper` file

To use the template in OC, we need a `jasper` file. To generate it we need build our project.

## Extract the Dataset from OC

In the OC application, go to the `File Explored`. Inside the `invoices` folder we can find a XML files with the entire Dataset used to generate the invoice. Also if you access to the invoice in `Billing/Invoices` you can download the XML of any invoice.

## Invoice structure

The invoice is divided in blocks to be more usable the pieces.
If we open the `invoice.jrxml` file, we can find:

* Header - Company logo
* Invoice number and data
* Partner information
* BLOCK with the subscriptions overview and price
* BLOCK with TAXES
* BLOCK with subscription details
* Footer

In any block jasper can loop to print all the instances of subscriptions, taxes or subscription details.

The mapping with the files are:

```
├── invoice.jrxml                 -- The all invoice where the blocks are used.
├── invoice_categories.jrxml      -- The subscriptions overview and prices.
├── invoice_tva.jrxml             -- The TAXES details
└── invoice_detail.jrxml          -- The subscriptions details.
```

## Localization

To translate the invoice, we can use the resources availables in Jasper. We can define a `properties` file with the locale covered with it and define the keys and values used in our template.

Inside the templates, we need define the `resourceBundle` attribute inside the `jasperRepor` tag.

With the `resourceBundle` set and the `properties` file with the key value of the translations, we can start to use the keys in the template with the next syntaxis:

```
$R{my-translated-key}
```

We define 2 languages + default:

* Default -- `i18n.properties`
* Catalan -- `i18n_ca.properties`
* Spanish -- `i18n_es.properties`

### OpenCell usage

To use this locales in an OpenCell environment, you need move the `i18n*.properties` files to the `/opt/jboss/` folder.
Otherwise the PDF generation fails.
